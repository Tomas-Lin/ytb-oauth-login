/*
 * @Mongoose simple demo
 * @Date: 2014-06-07
 * @Desc: Connect to Youtube office web
 *
 * @param Cat{constroctor}: Set the Field and content type
 * @param app{object}" express 回傳的物件
 * @param req.query.code 是oauth 的key，此值為固定，可以寫成Middleware要求token
 *
 *
 * */
var express = require('express'),
    mongoose = require('mongoose'),
    request = require('request'),
    app = express(),
    ytb = require("youtube-api"),
    credentials = require("./credentials"),
    authUrl = "https://accounts.google.com/o/oauth2/auth?";

mongoose.connect('mongodb://localhost/test');


for (var key in credentials) {
    if(key ==='client_secret'){ continue }
    authUrl += "&" + key + "=" + credentials[key];
    }
app.set('view engine', 'jade');


app.get('/', function(req, res){
    res.writeHead(302, {
        "Location": authUrl
    });
    res.end();
});

app.get('/oauth2callback', function(req, res){
    var url = 'https://accounts.google.com/o/oauth2/token',
        data = {form:{code: req.query.code,
            client_id: credentials.client_id,
            client_secret: credentials.client_secret,
            redirect_uri: credentials.redirect_uri,
            grant_type: 'authorization_code'}}


    console.log(req.query.code);
    request.post(url, data, function(err, resp, body){
        if(err){
            console.log(err);
        }else{
            data = JSON.parse(resp.body);
            ytb.authenticate({
                "type": "oauth",
                "token": data.access_token
            });
            ytb.playlists.list({
                "part": "snippet",
                "id":   "PLHzXa_F24ltIiKLvIab-ZWUMQZxLoiLa6",
                "mySubscribers": true,
                "maxResults": 50
            }, function(err, data){
                console.log(err);
            });//*/

        }
    });
    res.redirect('/singlelist');

});

app.get('/singlelist', function(req, res){
    res.send('hello')
});
app.listen(6555);
